class TreeIndexer:
	
	def __init__(self):
		self.index = {}
	
	def addElement(self,key,node):
		self.index[key] = node
	
	def getElement(self,key):
		return self.index[key]
