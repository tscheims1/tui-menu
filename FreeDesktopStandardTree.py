from TreeNode import TreeNode
from DesktopFile import DesktopFile
from Tree import Tree
from TreeIndex import TreeIndex
from Menu import Menu

class FreeDesktopStandardTree(Tree):
	
	def __init__(self):
		self.root = TreeNode(None)
		self.treeIndex = TreeIndex()
		mainCategories = ['AudioVideo','Audio','Video','Development','Education','Game','Graphics','Network','Office','Settings','System','Utility']
		
		
		for category in mainCategories:
			d = DesktopFile()
			d.name = category
			
			t = TreeNode(self.root)
			t.data = d 
			self.treeIndex.addElement(str(t.data),t)
			
			
			self.root.addChild(t)
		
	def addElement(self,desktopFile):
		
		for category in desktopFile.categories:
			parentNode = self.treeIndex.getElement(category)
			
			if(parentNode is not None):
				t = TreeNode(parentNode)
				t.data = desktopFile
				self.treeIndex.addElement(str(t.data),t)
				parentNode.addChild(t)
	def getNodes(self,key):
		entry = self.treeIndex.getElement(key)
		return entry.childs
		

		
