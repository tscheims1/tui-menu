from FreeDesktopStandardTree import FreeDesktopStandardTree
from Menu import Menu
from curses import wrapper
import curses
import os
import sys
import subprocess
import time
class TuiMenu:
	
	def __init__(self,stdscr):
		menu = Menu()
		menu.createMenuObjects()		
		self.freeDesktopStandardTree = FreeDesktopStandardTree()
		for entry in menu.entries:
			self.freeDesktopStandardTree.addElement(entry)
		self.stdscr = stdscr
		curses.cbreak()
		curses.noecho()
		#self.stdscr.keypad(1)
		self.windows = []
		self.borderSize =3
		
		
	def getMainMenu(self):
		
		return self.freeDesktopStandardTree.getTreeLevel(1)
	
			
	def printText(self,level,text,posy,posx,colorMode):
		self.windows[level].addstr(posy,posx,text, curses.color_pair(colorMode))
		self.windows[level].refresh()
		self.windows[level].move(posy,posx)
	def printMenuLevel(self,startX,level,key):
		
		if(level == 0):
			entries = self.freeDesktopStandardTree.getTreeLevel(1)
		else:
			entries = self.freeDesktopStandardTree.getNodes(key)
			
		if(len(entries) == 0):
			return
		
		maxX = 0
		if(len(self.windows) == level):
			
			for i in range(0,len(entries)):
				maxX = max(maxX,len(str(entries[i].data))+3)
			
			self.windows.append(curses.newwin(len(entries)+self.borderSize*2,maxX+self.borderSize*2,0,startX))
		self.windows[level].border()
		curses.cbreak()
		curses.noecho()
	
		
		for i in range(0,len(entries)):
			self.printText(level,str(entries[i]),i+self.borderSize,self.borderSize,1)

		
		self.printText(level,str(entries[0].data),self.borderSize,self.borderSize,2)
		self.windows[level].border()	
		self.windows[level].keypad(1)
		maxEntry = len(entries)
		y = 0
		
		
		while True:
			c = self.windows[level].getch()		
			
			#self.printText(level,str(c),0,0,1)
			
			if( c == curses.KEY_RESIZE):
				self.printText("resize....",0,0,1)
			
			
			if(c == curses.KEY_UP):#key-up
				if(y>0):
					
					self.windows[level].move(y-1+self.borderSize,self.borderSize)
					self.printText(level,str(entries[y]),y+self.borderSize,self.borderSize,1)
					self.printText(level,str(entries[y-1]),y-1+self.borderSize,self.borderSize,2)
					y-=1
			if(c == curses.KEY_DOWN):#key-up
				if(y < maxEntry-1):
					self.windows[level].move(y+1+self.borderSize,self.borderSize)
					self.printText(level,str(entries[y]),y+self.borderSize,self.borderSize,1)
					self.printText(level,str(entries[y+1]),y+1+self.borderSize,self.borderSize,2)
					
					y+=1
			if( c== curses.KEY_RIGHT):#key-right
				self.printMenuLevel(startX+20,level+1,str(entries[y].data))
			
			if(c == curses.KEY_LEFT):#key-left
				if(level != 0):
					self.windows[level].clear()
					self.windows[level].refresh()
					del self.windows[level]
					return
			if(c == c == 10 or c == 13):
				
				if(entries[y].data.execCommand is not None):
					subprocess.Popen(entries[y].data.execCommand,preexec_fn=os.setpgrp)
					sys.exit()
					
			if(c == 27):
				sys.exit()
				#n = self.windows[level].getch()
				#if(n == -1):
				#	sys.exit()
		
def main(stdscr):

	stdscr.clear()
	curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
	curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_MAGENTA)
	curses.init_pair(3,curses.COLOR_BLACK,curses.COLOR_BLACK)
	curses.curs_set(False)
	tui = TuiMenu(stdscr)
	
	tui.printMenuLevel(0,0,'')
	
	
	


		
		
		

wrapper(main)
