class Tree:
	
	def printTree(self):
		self.printTreeRecursion(self.root,'')
	def printTreeRecursion(self,node,intent):
		
		print(intent+str(node.data))
		for child in node.childs:
			self.printTreeRecursion(child,intent+'	')
		

	def printTreeLevel(self,level):
		self.printTreeLevelRecursion(self.root,0,level)
	
	def printTreeLevelRecursion(self,node,depth,level):

		if(level == depth):
			print(str(node.data))
		else:
			for child in node.childs:
				self.printTreeLevelRecursion(child,depth+1,level)
					
	def getTreeLevel(self,level):
		entries = []
		self.getTreeLevelRecursion(self.root,0,level,entries)
		
		return entries
		
	def getTreeLevelRecursion(self,node,depth,level,entries):

		if(level == depth):
			entries.append(node)
		else:
			for child in node.childs:
				self.getTreeLevelRecursion(child,depth+1,level,entries)
