import glob
from os.path import expanduser
from DesktopFileParser import DesktopFileParser
class Menu:
	
	def __init__(self):
		
		files = [f for f in glob.glob('/usr/share/applications/*.desktop')]
		files+= [f for f in glob.glob(expanduser("~")+'/.local/share/applications/*.desktop')] 

		self.contents = []
		self.entries = []
		self.desktopFileParser = DesktopFileParser()
		
		
		for f in files:
			with open(f, 'r') as fileContent:
				self.contents.append(fileContent.read())
	def createMenuObjects(self):
		for content in self.contents:
			desktopFile = self.desktopFileParser.parse(content)
			if(desktopFile.noDisplay == False):
				self.entries.append(desktopFile)
			
			
		
