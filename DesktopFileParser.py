import re
from DesktopFile import DesktopFile

class DesktopFileParser:
	
	def parse(self,content):
		
		name = ''
		comment = ''
		categories = []
		noDisplay = False
		execCommand = None
		
		nameGroups = re.search('Name=(.*)', content)
		commentGroups = re.search('Comment=(.*)',content)
		categorieGroups = re.search('Categories=(.*)',content)
		noDisplayGroups = re.search('NoDisplay=true',content)
		execGroups = re.search('Exec=(.*)',content)
		
		if(nameGroups  is not None):
			name  = nameGroups.group(1)
		if(commentGroups  is not None):
			comment = commentGroups.group(1)
		if(categorieGroups  is not None):
			categories = categorieGroups.group(1).split(';')
		if(noDisplayGroups is not None):
			noDisplay = True
		if(execGroups is not None):
			execCommand = execGroups.group(1).replace('%U','').replace('%u','').replace('%F','').replace('%f','').strip()
		
		
		d = DesktopFile()
		d.name = name
		d.comment = comment
		d.categories = categories
		d.noDisplay = noDisplay
		d.execCommand = execCommand
		
		return d
